import os

def extract_dpkg(task, env, args):
    if len(args) != 2:
        usage = "extract_dpkg <directory> <dpkg path>"
        raise ValueError(f"Not enough arguments: {usage}")

    directory = args[0]
    dpkg_path = args[1]

    assert os.path.isdir(directory), f"{direcory} is not a directory"
    assert os.path.isfile(dpkg_path), f"{dpkg_path} is not a file"

    res = os.system(f"dpkg-deb -xv '{dpkg_path}' '{directory}'")

    if res != 0:
        raise Exception("Failed to extract RPM", res)
